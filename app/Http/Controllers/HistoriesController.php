<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateHistoriesRequest;
use App\Http\Requests\EditHistoriesRequest;
use App\Models\Histories;
use Illuminate\Http\Request;


class HistoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $histories = Histories::paginate(10);
        return view('histories.index', compact('histories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('histories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateHistoriesRequest $request)
    {
        Histories::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_name' => $request->user_name
        ]);

        flash("Histories created successfully");
        return redirect()->route('histories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Histories  $histories
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $histories = Histories::findOrFail($id);
        // $histories = Histories::all();
        return view('histories.show', compact('histories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Histories  $histories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $histories = Histories::findOrFail($id);
        return view('histories.edit', compact('histories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Histories  $histories
     * @return \Illuminate\Http\Response
     */
    public function update(EditHistoriesRequest $request, Histories $histories, $id)
    {
        $validatedData = $request->validate([
           'title' => 'required|max:255',
           'description' => 'required',
           'user_name' => 'required'
        ]);
        Histories::whereId($id)->update($validatedData);

       return redirect('/histories')->with('success', 'Histories Data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Histories  $histories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $histories = Histories::findOrFail($id);
        $histories->delete();
        flash("Histories Deleted Successfully");
        return redirect()->back();
    }
}

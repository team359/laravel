<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateHistoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
           'title' => 'required|max:255|unique:histories,title',
           'description' => 'required',
           'user_name' => 'required'
      ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Please enter history title.',
            'title.unique' => 'The title has already been taken.',
            'description.required' => 'Please enter history description.',
            'user_name.required' => 'Please enter user name.'
        ];
    }

}

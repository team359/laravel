<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditHistoriesRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|max:255|',
            'description' => 'required',
            'user_name' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please enter title.',
            'title.unique' => 'The title has already been taken.',
            'description.required' => 'Please enter description.',
            'user_name.required' => 'Please enter user name.'
        ];
    }
}

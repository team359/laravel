@extends('layouts.master')

@section('title') Edit Histories @endsection

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Histories
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('histories.update', $histories->id ) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="title">Title</label>
              <input type="text" class="form-control" name="title" value="{{ $histories->title }}"/>
          </div>
          <div class="form-group">
              <label for="description">Description</label>
              <input type="text" class="form-control" name="description" value="{{ $histories->description }}"/>
          </div>
          <div class="form-group">
              <label for="user_name">User Name</label>
              <input type="text" class="form-control" name="user_name" value="{{ $histories->user_name }}"/>
          </div>
          <button type="submit" class="btn btn-primary">Update</button>
          <a href="http://localhost:8000/histories" class="btn btn-default">Cancel</a>
      </form>
  </div>
</div>
@endsection

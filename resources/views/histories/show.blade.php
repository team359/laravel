@extends('layouts.master')

@section('title') View Histories @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('histories.index') }}" class="btn btn-info pull-right mb-3">Back</a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th>Title</th>
                <td>{{ $histories->title }}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td>{{ $histories->description }}</td>
            </tr>
            <tr>
                <th>User Name</th>
                <td>{{ $histories->user_name }}</td>
            </tr>
            <tr>
                <th>Updated At</th>
                <td>{{ $histories->updated_at->diffForHumans() }}</td>
            </tr>
            <tr>
                <th>Created At</th>
                <td>{{ $histories->created_at->diffForHumans() }}</td>
            </tr>
        </table>
    </div>
@endsection

@extends('layouts.master')

@section('title') Create Histories @endsection

@section('content')
    <form action="{{ route('histories.store') }}" method="post">
        @csrf
        <div class="card card-default">
            <div class="card-header">
                Create Histories
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control @if($errors->has('title')) is-invalid @endif" name="title" value="{{ old('title') }}">
                    @if($errors->has('title'))
                        <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea cols="5" rows="3" class="form-control @if($errors->has('description')) is-invalid @endif" name="description">{{ ('') }}</textarea>
                    @if($errors->has('description'))
                        <div class="invalid-feedback">{{ $errors->first('description') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="user_name">User name</label>
                    <input type="text" class="form-control @if($errors->has('user_name')) is-invalid @endif" name="user_name" value="{{ old('user_name') }}">
                    @if($errors->has('user_name'))
                        <div class="invalid-feedback">{{ $errors->first('user_name') }}</div>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary" type="submit">Save</button>
                <a href="{{ route('histories.index') }}" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </form>
@endsection

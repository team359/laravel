@extends('layouts.master')

@section('title') Histories @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('histories.create') }}" class="btn btn-primary pull-right mb-3">Add New</a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>User Name</th>
                <th>Updated At</th>
                <th>Created At</th>
            </tr>
            </thead>
            <tbody>
            @forelse($histories as $histori)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $histori->title }}</td>
                    <td>{{ $histori->description }}</td>
                    <td>{{ $histori->user_name }}</td>
                    <td>{{ $histori->updated_at->diffForHumans() }}</td>
                    <td>{{ $histori->created_at->diffForHumans() }}</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a href="{{ route('histories.edit', $histori->id)}}" type="button" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('histories.show', $histori->id) }}" type="button" class="btn btn-info"><i class="fa fa-eye"></i></a>

                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_histori_{{ $histori->id }}">
                                <i class="fa fa-trash"></i>
                            </button>

                            <div class="modal fade" id="delete_histori_{{ $histori->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form action="{{ route('histories.destroy', $histori->id) }}" id="form_delete_histori_{{ $histori->id }}" method="post">
                                        @csrf
                                        @method('DELETE');
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure want to delete "<b>{{ $histori->title }}</b>" histori?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                <button type="submit" class="btn btn-danger">Yes! Delete It</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" align="center">No Histories Found.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    <div class="row">
        {{ $histories->links() }}
    </div>
@endsection
